# tau-fact

A tool for exploring the elasticity (ε) of τ-factorizations in integral domains.

## Background

Elasticity (ε) can be defined as the longest atomic factorization divided by the shortest
atomic factorizaiton.
